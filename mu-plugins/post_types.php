<?php

/*
    Register Book Post Type
*/
function Books_post_type()
{

    $labels = array(
        'name' => __('books', 'book_theme'),
        'singular_name' => __('book', 'book_theme'),
        'menu_name' => __('Books', 'book_theme'),
        'parent_item_colon' => __('book:', 'book_theme'),
        'all_items' => __('All Books', 'book_theme'),
        'view_item' => __('View Book', 'book_theme'),
        'add_new_item' => __('Add New Book', 'book_theme'),
        'add_new' => __('Add New', 'book_theme'),
        'edit_item' => __('Edit Book', 'book_theme'),
        'update_item' => __('Update Book', 'book_theme'),
        'search_items' => __('Search Book', 'book_theme'),
        'not_found' => __('Not Found', 'book_theme'),
        'not_found_in_trash' => __('Not Found In Trash', 'book_theme')
    );
    $args = array(
        'description' => 'books',
        'labels' => $labels,
        'supports' => array('title', 'editor', 'page-attributes', 'thumbnail', 'comments', 'author'),
        'taxonomies' => array('author_cat', 'publisher_cat'),
        'hierarchical' => FALSE,
        'public' => TRUE,
        'show_ui' => TRUE,
        'show_in_menu' => TRUE,
        'show_in_nav_menus' => TRUE,
        'show_in_admin_bar' => TRUE,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-book-alt',
        'can_export' => TRUE,
        'has_archive' => TRUE,
        'exclude_from_search' => TRUE,
        'publicly_queryable' => TRUE,
        'rewrite' => array('slug' => 'book', 'with_front' => false),
        'capability_type' => 'post',
    );

    register_post_type('book', $args);
}


add_action('init', 'Books_post_type');


/*
    Register Book's Author Category
*/

function register_taxonomy_author_cat()
{

    $labels = array(
        'name' => __('Book Authors', 'book_theme'),
        'singular_name' => 'Author',
        'search_items' => __('Search Author', 'book_theme'),
        'popular_items' => __('Popular Author', 'book_theme'),
        'all_items' => __('All Authors', 'book_theme'),
        'parent_item' => __('Parent Author', 'book_theme'),
        'parent_item_colon' => __('Parent Author:', 'book_theme'),
        'edit_item' => __('Edit Author', 'book_theme'),
        'update_item' => __('Update Author', 'book_theme'),
        'add_new_item' => __('Add new Author', 'book_theme'),
        'new_item_name' => __('Add Author Name', 'book_theme'),
        'separate_items_with_commas' => __('Separate Authors With Commas', 'book_theme'),
        'add_or_remove_items' => __('Add Or Remove Author', 'book_theme'),
        'choose_from_most_used' => __('Choose from Most Used', 'book_theme'),
        'menu_name' => __('Authors', 'book_theme')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'author_cat'),
        'query_var' => true
    );

    register_taxonomy('author_cat', array('book'), $args);
}

add_action('init', 'register_taxonomy_author_cat');


/*
    Register Book's Publisher Category
*/


function register_taxonomy_publisher_cat()
{

    $labels = array(
        'name' => __('Book Publishers', 'book_theme'),
        'singular_name' => 'Publisher',
        'search_items' => __('Search Publisher', 'book_theme'),
        'popular_items' => __('Popular Publisher', 'book_theme'),
        'all_items' => __('All Publishers', 'book_theme'),
        'parent_item' => __('Parent Publisher', 'book_theme'),
        'parent_item_colon' => __('Parent Publisher:', 'book_theme'),
        'edit_item' => __('Edit Publisher', 'book_theme'),
        'update_item' => __('Update Publisher', 'book_theme'),
        'add_new_item' => __('Add new Publisher', 'book_theme'),
        'new_item_name' => __('Add Publisher Name', 'book_theme'),
        'separate_items_with_commas' => __('Separate Publishers With Commas', 'book_theme'),
        'add_or_remove_items' => __('Add Or Remove Publisher', 'book_theme'),
        'choose_from_most_used' => __('Choose from Most Used', 'book_theme'),
        'menu_name' => __('Publishers', 'book_theme')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'publisher_cat'),
        'query_var' => true
    );

    register_taxonomy('publisher_cat', array('book'), $args);
}

add_action('init', 'register_taxonomy_publisher_cat');


/**
 *
 * Create Custom field ISBN
 *
 *
 */

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5ba40503694ce',
        'title' => 'ISBN',
        'fields' => array(
            array(
                'key' => 'field_5ba4050922814',
                'label' => 'ISBN',
                'name' => 'isbn',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'book',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

/**
 *
 *  Create Action Api Notification
 *
 */

add_filter('acf/update_value/name=isbn', 'my_check_for_change', 10, 3);
function my_check_for_change($value, $post_id, $field) {
    global $wpdb;
    $old_value = get_post_meta($post_id, 'isbn', true);
    if ($old_value != $value) {
        if($wpdb->get_results("select * from wp_books where post_id = $post_id")){
            $wpdb->update('wp_books', array(
                'isbn' => $value
            ), array('post_id' => $post_id));
        }else {
            $wpdb->insert('wp_books', array(

                'post_id' => $post_id,
                'isbn' => $value
            ));
        }
    }
    return $value;
}

