<?php
    /**
     * The header for our theme
     *
     * This is the template that displays all of the <head> section and everything up until <div id="content">
     *
     * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package books_theme
     */

    ?>
    <!doctype html>
    <html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
    <header>
        <nav class="navbar navbar-default remove-margin-navbar">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">Books Themes</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class=<?php if(is_home() && !is_single()){ echo "active";}?>><a href="<?php echo get_home_url(); ?>">Home<span class="sr-only">(current)</span></a></li>
                        <li><a href="<?php echo home_url('/posts')?>">Posts<span class="sr-only">(current)</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false">Books<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php $terms = get_terms('');?>
                                <li><a href="<?php echo get_post_type_archive_link('book')?>">Books Page</a></li>
                                <li><a href="<?php echo home_url('author_cat')?>">Books Author</a></li>
                                <li><a href="<?php echo home_url('publisher_cat')?>">Books Publishers</a></li>
                                <li><a href="<?php echo home_url('books-info')?>">Books Info</a></li>

                            </ul>
                        </li>
                    </ul>


                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
       <?php if(!is_single()) {
           get_template_part( 'template-parts/header', 'background' );
       }?>
    </header>
    <div id="content" class="site-content">
        <div class="container">
            <div class="row">
