<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package books_theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="jumbotron bx-sh">
                <div class="container">


		<?php
		while ( have_posts() ) :
			the_post(); ?>
                <div class="content">
                    <div class="content__inner">
                        <div class="content__inner__img pull-right"><?php the_post_thumbnail('medium', array(
                                'class' => 'img-radius'
                            ))?></div>
                        <div class="content__inner__title"><h1><?php the_title()?></h1></div>
                        <div class="content__inner__desc"><?php the_content()?></div>
                    </div>
                </div>

			<?php

		endwhile; // End of the loop.
		?>
                </div>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
