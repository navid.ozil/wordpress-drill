


<?php get_header()?>
<?php $post_type = get_post_type_object(get_post_type());
if ($post_type) {
    $post_type_name  = $post_type->labels->menu_name;
}
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="card-view-posts">
            <h2 class="card-view-posts__title"><?php  echo $post_type_name?></h2>
            <?php
            $posts = new WP_QUERY(array(
                'post_type' => 'book'
            ));
            if ($posts->have_posts()) :

                while ($posts->have_posts()) :
                    $posts->the_post();
                    get_template_part('template-parts/content', get_post_type());

                endwhile;
            endif;
            ?>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer();?>
