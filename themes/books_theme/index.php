<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package books_theme
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="card-view-posts">
                <h2 class="card-view-posts__title">Posts</h2>
                <?php
                if (have_posts()) :

                    while (have_posts()) :
                        the_post();
                        get_template_part('template-parts/content', get_post_type());

                    endwhile;
                    the_posts_navigation();
                else :

                    get_template_part('template-parts/content', 'none');
                endif;
                ?>
            </div>
            <!--book post type -->
            <div class="card-view-posts">
                <h2 class="card-view-posts__title">Books</h2>
                <?php
                $book_posts = new WP_QUERY(array(
                        'post_type' => 'book',
                        'posts_per_page' => 8
                ));
                if ($book_posts->have_posts()) :

                    if (is_home() && !is_front_page()) :?>
                    <?php endif;

                    while ($book_posts->have_posts()) :
                        $book_posts->the_post();
                        get_template_part('template-parts/content', get_post_type());

                    endwhile;
                    the_posts_navigation();
                else :

                    get_template_part('template-parts/content', 'none');
                endif;
                ?>
            </div>
            <!--end book post type-->
        </main><!-- #main -->
    </div><!-- #primary -->


<?php

get_footer();
