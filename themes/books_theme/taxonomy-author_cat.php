<?php
$taxonomy = get_queried_object();
$term_id = $taxonomy->term_id;
?>


<?php get_header()?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="card-view-posts">
                <h2 class="card-view-posts__title">Posts</h2>
                <?php
                        $books_tax = new WP_QUERY(array(
                                'post_type' => 'book',
                                'tax_query' => array(
                                array(
                                    'taxonomy' => 'author_cat',
                                    'field' => 'term_id',
                                    'terms' => $term_id,
                                ),
                            ),
                        ));
                if($books_tax->have_posts()) {
                    while ($books_tax->have_posts()) :
                        $books_tax->the_post();
                        get_template_part('template-parts/content', get_post_type());

                    endwhile;
                }else{ ?>
                    <h4 class="alert alert-info">No Posts published yet with <strong><?php the_archive_title()?></strong></h4>
                <?php }
                ?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();?>