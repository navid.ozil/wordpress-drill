<?php
/*
 Template Name: Books Info
 */
?>
<?php get_header(); ?>
    <div class="container">
    <div class="row">
        <div class="book_info_wrapper">
            <div class="col-md-9">
                <div class="book_info_table">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Post_id</td>
                            <td>ISBN</td>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        <?php
                        global $wpdb;
                        $books_info = $wpdb->get_results("select * from wp_books");
                        $json_books_info = json_encode($books_info);
                        foreach ($books_info as $book_info) {
                            ?>
                            <tr>
                                <td><?php echo $book_info->ID ?></td>
                                <td><?php echo $book_info->post_id ?></td>
                                <td><?php echo $book_info->isbn ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <div class="book_info_input">
                    <div class="form-group">
                        <label for="searchInput" class="small l">Real Time Search</span>
                        <input id="searchInput" onkeyup="start()" value="" type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let start = function () {
            setTimeout(findResult, 700)
        };

        function findResult() {
            let getInput = function () {
                document.getElementById("tbody").innerHTML = "";
                return document.getElementById("searchInput").valueOf().value;

            };
            let lists = (<?php echo $json_books_info?>);
            let valueInput = getInput();
            for (list of lists) {
                if (list.isbn.includes(valueInput)) {
                    document.getElementById("tbody").innerHTML +=
                        `<td>${list.ID}</td><td>${list.post_id}</td><td>${list.isbn}</td>`
                }
            }
        }

    </script>

    </div><?php get_footer() ?>