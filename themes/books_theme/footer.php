<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package books_theme
 */

?>
</div>
</div>
</div><!-- #content -->

<footer id="colophon" class="site-footer">
    <div class="background_footer">
        <div class="container">
            <div class="row">
                <span class="title">Footer</span>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
