<?php
/*
Template Name: Archives
*/
?>


<?php get_header()?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="card-view-posts">
            <h2 class="card-view-posts__title">Posts</h2>
            <?php
            if(is_page('posts')){
                $archive_posts = new WP_QUERY(array(
                        'post_type' => 'post'
                ));
                if ($archive_posts->have_posts()) {
                    while ($archive_posts->have_posts()) :
                        $archive_posts->the_post();
                        get_template_part('template-parts/content', get_post_type());
                    endwhile;
                } else { ?>
            }else {
                if (have_posts()) {
                    while (have_posts()) :
                        the_post();
                        get_template_part('template-parts/content', get_post_type());
                    endwhile;
                } else { ?>
                    <h4 class="alert alert-info">No Posts published yet with
                        <strong><?php the_archive_title() ?></strong></h4>
                <?php }
            }
            ?>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();?>
