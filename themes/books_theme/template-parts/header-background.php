<?php
$page_title = "";
if(is_home() && !is_single()){
    $page_title = "Home Page";
}else{
    if(is_archive()){
        $page_title = get_the_archive_title();
    }
    elseif (is_page()){
        $page_title = "Archive: ". get_the_title();
    }
}
?>

<div class="background_header">
    <div class="background_header__img" style="background-image: url(<?php echo get_template_directory_uri() ?>./images/library-bg.jpg)">
    </div>
    <h1 class="background_header__title"><?php echo $page_title?></h1>
</div>