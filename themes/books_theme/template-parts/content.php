<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package books_theme
 */

?>
<div class="col-x2-12 col-sm-4 col-md-3">
    <article class="item" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="item__img">
            <?php the_post_thumbnail('card-view', array(
                'class' => 'img'
            ));
            ?>
            <?php $category = get_the_category();
                if($category){ ?>
            <div class="item__img-category">
                <a href="<?php echo get_category_link($category[0]->cat_ID) ?>"><?php echo($category[0]->cat_name); ?></a>
            </div>
           <?php } ?>
        </div>
        <a href="<?php the_permalink(); ?>">
            <div class="item__desc">
                <div class="title"><h4><?php the_title(); ?></h4></div>
                <div class="text-center time"><span class="glyphicon glyphicon-time"></span><?php echo get_the_date('F j, Y');?></div>
                <span class="goto"><span class="glyphicon glyphicon-arrow-right"></span></span>
            </div>
        </a>
    </article>
</div>  <!-- #post-<?php the_ID(); ?> -->
